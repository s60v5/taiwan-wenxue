import React, { useState } from "react"
import PropTypes from "prop-types"
import { Link } from "gatsby"

const Header = () => {
  const [menu, triggerMenu] = useState(false)
  return (
    <header>
      <div className="menu-wrapper">
        <h1 style={{ margin: 0 }}>
          <Link
            to="/"
            style={{
              color: `white`,
              textDecoration: `none`,
            }}
          >
            竹<span>APP</span>
          </Link>
        </h1>
        <button
          className={
            "hamburger hamburger--collapse " + (menu ? "is-active" : "")
          }
          onClick={() => triggerMenu(!menu)}
          type="button"
        >
          <span className="hamburger-box">
            <span className="hamburger-inner"></span>
          </span>
        </button>
        <div className={"menu " + (menu ? "is-active" : "")}>
          <ul>
            <li>
              <Link to="/">首頁</Link>
            </li>
            <li>
              <Link to="/page1">逢甲竹枝詞</Link>
            </li>
            <li>
              <Link to="/page10">鰲峰竹枝詞賞析</Link>
            </li>
            <li>
              <Link to="/page2">逢甲竹枝詞影片</Link>
            </li>
            <li>
              <Link to="/page3">地方竹枝詞</Link>
            </li>
            <li>
              <Link to="/page4">地方竹枝詞</Link>
            </li>
            <li>
              <Link to="/page5">鹿港踏查記錄</Link>
            </li>
            <li>
              <Link to="/page6">歲時節慶</Link>
            </li>
            <li>
              <Link to="/page7">風土民情</Link>
            </li>
            <li>
              <Link to="/page8">文化伴手禮</Link>
            </li>
            <li>
              <Link to="/page9">竹枝詞地理空間</Link>
            </li>
          </ul>
        </div>
      </div>
    </header>
  )
}

Header.propTypes = {
  siteTitle: PropTypes.string,
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header
