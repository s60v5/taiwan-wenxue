import React from "react"
import { GatsbyImage } from "gatsby-plugin-image"
import { Link, graphql, useStaticQuery } from "gatsby"

const GridItem = ({ title, subtitle, image, link }) => {
  const images = useStaticQuery(graphql`
    {
      page1: file(relativePath: { eq: "page-1.jpg" }) {
        childImageSharp {
          gatsbyImageData(
            placeholder: BLURRED
            formats: [AUTO, WEBP]
            quality: 100
          )
        }
      }
      page2: file(relativePath: { eq: "page-2.jpg" }) {
        childImageSharp {
          gatsbyImageData(
            placeholder: BLURRED
            formats: [AUTO, WEBP]
            quality: 100
          )
        }
      }
      page3: file(relativePath: { eq: "page-3.jpg" }) {
        childImageSharp {
          gatsbyImageData(
            placeholder: BLURRED
            formats: [AUTO, WEBP]
            quality: 100
          )
        }
      }
      page4: file(relativePath: { eq: "page-4.jpg" }) {
        childImageSharp {
          gatsbyImageData(
            placeholder: BLURRED
            formats: [AUTO, WEBP]
            quality: 100
          )
        }
      }
      page5: file(relativePath: { eq: "page-5.jpg" }) {
        childImageSharp {
          gatsbyImageData(
            placeholder: BLURRED
            formats: [AUTO, WEBP]
            quality: 100
          )
        }
      }
      page6: file(relativePath: { eq: "page-6.jpg" }) {
        childImageSharp {
          gatsbyImageData(
            placeholder: BLURRED
            formats: [AUTO, WEBP]
            quality: 100
          )
        }
      }
      page7: file(relativePath: { eq: "page-7.jpg" }) {
        childImageSharp {
          gatsbyImageData(
            placeholder: BLURRED
            formats: [AUTO, WEBP]
            quality: 100
          )
        }
      }
      page8: file(relativePath: { eq: "page-8.png" }) {
        childImageSharp {
          gatsbyImageData(
            placeholder: BLURRED
            formats: [AUTO, WEBP]
            quality: 100
          )
        }
      }
      page9: file(relativePath: { eq: "page-9.jpg" }) {
        childImageSharp {
          gatsbyImageData(
            placeholder: BLURRED
            formats: [AUTO, WEBP]
            quality: 100
          )
        }
      }
    }
  `)

  if (link == "page1") {
    return (
      <div className="grid-item">
        <div className="grid-item__image">
          <GatsbyImage
            image={images[image]?.childImageSharp?.gatsbyImageData}
            alt={title}
          />
        </div>
        <div className="grid-item-content">
          <p>{title}</p>
          <Link to="/page1">逢甲竹枝詞</Link>
          <Link to="/page10">鰲峰竹枝詞</Link>
        </div>
      </div>
    )
  }

  return (
    <Link className="grid-item" to={link}>
      <div className="grid-item__image">
        <GatsbyImage
          image={images[image]?.childImageSharp?.gatsbyImageData}
          alt={title}
        />
      </div>
      <div className={"grid-item-content " + link}>
        <p>{title}</p>
        <p>{subtitle}</p>
      </div>
    </Link>
  )
}

export default GridItem
