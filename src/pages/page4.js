import * as React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import Seo from "../components/seo"

const Page4 = () => (
  <Layout>
    <Seo title="地方竹枝詞" />
    <div className="yt-container">
    <iframe width="560" height="315" src="https://www.youtube.com/embed/KenpD9R2mfw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
    <Link className="back" to="/">返回首頁</Link>
  </Layout>
)

export default Page4
