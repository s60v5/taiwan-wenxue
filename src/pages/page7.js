import * as React from "react"
import { Link } from "gatsby"
import { StaticImage } from "gatsby-plugin-image"

import Layout from "../components/layout"
import Seo from "../components/seo"

const Page7 = () => (
  <Layout>
    <Seo title="風土民情" />
    <div className="article">
      <h1>清水三山國王</h1>
      <p>
        三山國王是發源於中國廣東粵東地區的信仰，指的是河婆鎮北面三座山的山神，三座山分別是獨山、明山、巾山。隨著當地移民向外擴展，慢慢成為東南亞、香港及台灣民間信仰之一，是潮州人移民的精神信仰。課本上總是說三山國王是客家人的信仰，但這個說法不算正確，三山國王主要是潮州人的信仰，潮州的客家人只是很小一小部分，況且潮州以外的客家人也有他們自己的信仰。會產生這樣的誤解，其實是因為隨著廟宇越來越靈驗，在這邊居住的所有族群也紛紛跟進改信三山國王，所以出現很多客家人信仰三山國王，創造出這樣的迷思。
      </p>
      <div className="image-container">
        <StaticImage
          src="../images/fengtu-1.jpg"
          placeholder="blurred"
          width={400}
        />
      </div>
      <p>
        臺灣人的先祖,決定移墾臺灣,就要選擇作為移墾地守護神,大部分的潮州人選擇將三山國王的神像帶來，或在家奉祀，或在臺灣的墾地建立寺廟奉祀，以祈求保佑、平安，是為移民的精神支持力量。等到經濟力量許可後，便會在僑居地建立家鄉式的寺廟作為回報。會選擇三山國王作為守護神，可能是因為臺灣原住民有「出草馘首」習俗（獵頭）。因為「三山國王」是「山神」,「山神」定能夠制伏「山中之生番」的聯想之下，而誕生的選擇。
      </p>
      <div className="image-container">
        <StaticImage
          src="../images/fengtu-2.jpg"
          placeholder="blurred"
          width={400}
        />
      </div>
      <p>
        根據臺灣鄉村習俗的記載，三山國王聖誕定為每年二月二十五。而粵東地區三山國王出遊則定在正月，一般是正月初十或十五。
      </p>

      <div className="image-container">
        <StaticImage
          src="../images/fengtu-3.jpg"
          placeholder="blurred"
          width={400}
        />
      </div>
      <p>
        三山國王的有不少傳說，一說是稱連傑、趙軒，喬俊結拜三兄弟，因保護代王侑，被封護國公，唐朝興起後隱居於三山，死後在三山各自顯聖，被視為山神。到了北宋時期神威顯赫，先後被封王爵、封號，即明山王、巾山王、獨山王。明山王為清化盛德報國王（大國王）、巾山王為助政明肅寧國王（二國王）、獨山國王為惠威弘應豐國王（三國王）。
      </p>
      <h1>三山國王廟</h1>
      <div className="image-container">
        <StaticImage
          src="../images/fengtu-4.jpg"
          placeholder="blurred"
          width={400}
        />
      </div>
      <p>清水三山國王廟，又稱為「調元宮」，位於臺中清水的鰲峰里。</p>
      <div className="image-container">
        <StaticImage
          src="../images/fengtu-5.jpg"
          placeholder="blurred"
          width={400}
        />
      </div>
      <p>
        據傳三山國王廟是清朝雍正年間建成，是一位張姓客籍人士所建，初建之時面前有一對石獅子，若有要還願的信徒們會將供品塞進張嘴的雄獅口中，然而現今已經不知去向。（此圖是重建後的石獅子。）
      </p>

      <div className="image-container">
        <StaticImage
          src="../images/fengtu-6.jpg"
          placeholder="blurred"
          width={400}
        />
      </div>
      <p>
        廟裡有一幅寫著「巍煥東瀛」的匾額，意旨高大光輝的日本。（筆者認為，雖然該廟是建於清朝，但後來廟宇在1935年重建，正好處於日治時期，極可能並非於清朝時所寫的匾額，故解釋成日本而非東洋）
      </p>
      <div className="image-container">
        <StaticImage
          src="../images/fengtu-7.jpg"
          placeholder="blurred"
          width={400}
        />
      </div>
      <p>
        該廟朴子里萬順宮有交陪關係，每年農曆10月25日均會舉辦象棋比賽，以慶祝清水三山國王聖誕千秋。（此為意示圖）
      </p>
    </div>
    <Link className="back" to="/">
      返回首頁
    </Link>
  </Layout>
)

export default Page7
