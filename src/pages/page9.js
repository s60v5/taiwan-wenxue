import * as React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import Seo from "../components/seo"

const Page9 = () => (
  <Layout>
    <Seo title="竹枝詞地理空間" />
    <div className="yt-container">
    <iframe src="https://www.google.com/maps/d/embed?mid=17T6qmaRQUWcc-DmfrdvKCpGk1r_5MDgt" width="560" height="315"></iframe>
    </div>
    <Link className="back" to="/">返回首頁</Link>
  </Layout>
)

export default Page9
