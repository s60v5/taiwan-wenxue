import * as React from "react"
import GridItem from "../components/grid-item"
import Layout from "../components/layout"
import Seo from "../components/seo"

const items = [
  {
    title: "作品賞析",
    subtitle: "",
    image: "page1",
  },
  {
    title: "逢甲竹枝詞",
    subtitle: "影像詩",
    image: "page2",
  },
  {
    title: "地方竹枝詞",
    subtitle: "介紹影片",
    image: "page3",
  },
  {
    title: "地方竹枝詞",
    subtitle: "吟唱影片",
    image: "page4",
  },
  {
    title: "鹿港踏查記錄",
    subtitle: "",
    image: "page5",
  },
  {
    title: "歲時節慶",
    subtitle: "Timeline",
    image: "page6",
  },
  {
    title: "風土民情",
    subtitle: "",
    image: "page7",
  },
  {
    title: "文化伴手禮",
    subtitle: "茶品牌",
    image: "page8",
  },
  {
    title: "竹枝詞地理空間",
    subtitle: "地圖標記",
    image: "page9",
  },
]

const IndexPage = () => (
  <Layout>
    <Seo title="Home" />
    <div className="grid-home">
      {items.map((item, index) => {
        return (
          <GridItem
            key={index}
            title={item.title}
            subtitle={item.subtitle}
            image={item.image}
            link={item.image}
          />
        )
      })}
    </div>
  </Layout>
)

export default IndexPage
