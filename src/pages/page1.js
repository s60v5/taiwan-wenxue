import * as React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import Seo from "../components/seo"

const Page2 = () => (
  <Layout>
    <Seo title="逢甲竹枝詞" />
    <div className="poem-container">
      <p class="title">
        夜市　<span>作者：林楷倫</span>
      </p>
      <p class="poem">
        七彩霓虹叫人醒，呼朋喚友快朵頤。
        <br />
        人聲鼎沸來相迎，興致高昂月影低。
      </p>
      <p class="desc">
        <span>解釋：</span>
        夜市的霓虹燈讓人們不自覺的前去，三五好友相約一起去夜市裡面吃吃喝喝，
        叫賣的聲音像是呼應著我們的吵鬧聲，高昂的興致月亮的影子都低了下頭。
      </p>
    </div>
    <div className="poem-container">
      <p class="title">
        春雨塘　<span>作者：陳菀寧</span>
      </p>
      <p class="poem">
        波光影動鴨聲喚，耀幻粼粼藻葉平。
        <br />
        倩草絲絲牽愁斷，唯獨酒盡踏歌聲。
      </p>
      <p class="desc">
        <span>解釋：</span>
        湖面上光影交錯又加上不絕的鴨聲帶給人動感的意象，
        浮耀的湖面幻影加上陽光帶來的波光，粼粼舞動，藻葉平鋪其中。
        <br />
        美好的水草一絲一絲的，牽著我的思愁，要斷不斷，無法放開，
        又揪著無法釋懷，我能做的，也只有把酒盡空，踏在湖邊默默唱起歌了。
      </p>
    </div>
    <div className="poem-container">
      <p class="title">
        鴨子 <span>作者：柯大衛</span>
      </p>
      <p class="poem">
        每逢入校池中唱，翠苗搖曳見白鴨。
        <br />
        初夏降至逢甲地，綠蔭處處聞聲呱。
      </p>
      <p class="desc">
        <span>解釋：</span>
        進到逢甲校園就會看到池塘有青草搖曳著，鴨子悠哉游泳並叫喚不停。 <br />
        夏天才剛降臨逢甲大學這塊土地，大樹下能遮陽的地方都有鴨子存在。
      </p>
    </div>
    <div className="poem-container">
      <p class="title">
        秋紅谷 <span>作者：劉郁秀</span>
      </p>
      <p class="poem">
        霜楓碧水絕塵世，錦鯉朱蕉意盎然。
        <br />
        萬丈高樓唯此靜，秋風謐夜儷人圓。
      </p>
      <p class="desc">
        <span>解釋：</span>
        秋天的楓葉和碧綠的湖水隔絕了外頭塵世的喧囂，湖裡的錦鯉和茂盛的朱蕉展示了這塊地的生機盎然。
        <br />
        在車水馬龍的城市裡，高樓萬起，待在秋紅谷中卻像是處在森林裡，清幽自在，心靈寧靜。
        <br />
        秋天的夜晚，微風吹拂，寧靜的氣氛，讓許多有情人在這裡相見，耳鬢廝磨，訴說著浪漫的想像與思念。
      </p>
    </div>
    <div className="poem-container">
      <p class="title">
        行道 <span>作者：洪瑋</span>
      </p>
      <p class="poem">
        販家吆喝聲不斷，旅者遊街夜未眠。
        <br />
        鐵馬千尋蜿蜒行，銅鈴一刻不得閒。
      </p>
      <p class="desc">
        <span>解釋：</span>
        逢甲商圈熱鬧無比，攤販的吆喝聲此起彼落，招攬客人。
        遊客披著月光，夜晚正適合這種繁榮氣氛，還有誰會想要睡覺呢？
        <br />
        但是逛街的時候，由於路窄不寬，與遊客一同擠在行道上的交通工具，
        尋找縫隙，穿梭在人群中，蜿蜒蛇行，添增一絲危險與刺激。
        <br />
        因此眼睛（銅鈴）要注意好路況，不可一時發呆或分心做其他事情，避免發生擦撞意外，造成憾事。
      </p>
    </div>
    <div className="poem-container">
      <p class="title">
        建築 <span>作者：林亭宇</span>
      </p>
      <p class="poem">
        紅牆綠瓦層堆疊，樹蔭長廊倚宇軒。 <br />
        學子吟詩遊其中，書聲琅琅繞亭園。
      </p>
      <p class="desc">
        <span>解釋：</span>
        建築都是由一磚一瓦堆砌而成，與校園內的樹木長廊互相對應著，有著和諧的美感。<br />
        逢甲的學生們每天穿梭在校園建築之中，書聲則是環繞於各建築之中。
      </p>
    </div>
    <Link className="back" to="/">
      返回首頁
    </Link>
  </Layout>
)

export default Page2
