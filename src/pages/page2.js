import * as React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import Seo from "../components/seo"

const Page2 = () => (
  <Layout>
    <Seo title="逢甲竹枝詞" />
    <div className="yt-container">
      <iframe width="560" height="315" src="https://www.youtube.com/embed/Fz2gtddedfU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
    <Link className="back" to="/">返回首頁</Link>
  </Layout>
)

export default Page2