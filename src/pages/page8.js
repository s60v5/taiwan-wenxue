import * as React from "react"
import { graphql, useStaticQuery, Link } from "gatsby"
import { GatsbyImage } from "gatsby-plugin-image"
import Slider from "react-slick"
import Layout from "../components/layout"
import Seo from "../components/seo"
import "slick-carousel/slick/slick.css"
import "slick-carousel/slick/slick-theme.css"
const settings = {
  infinite: true,
  speed: 500,
  slidesToShow: 3,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 900,
      settings: {
        slidesToShow: 2,
      },
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
      },
    },
  ],
}

const Page8 = () => {
  const images = useStaticQuery(graphql`
    {
      tea1: file(relativePath: { eq: "tea-1.png" }) {
        childImageSharp {
          gatsbyImageData(
            placeholder: BLURRED
            formats: [AUTO, WEBP]
            quality: 100
          )
        }
      }
      tea2: file(relativePath: { eq: "tea-2.png" }) {
        childImageSharp {
          gatsbyImageData(
            placeholder: BLURRED
            formats: [AUTO, WEBP]
            quality: 100
          )
        }
      }
      tea3: file(relativePath: { eq: "tea-3.png" }) {
        childImageSharp {
          gatsbyImageData(
            placeholder: BLURRED
            formats: [AUTO, WEBP]
            quality: 100
          )
        }
      }
      tea4: file(relativePath: { eq: "tea-4.png" }) {
        childImageSharp {
          gatsbyImageData(
            placeholder: BLURRED
            formats: [AUTO, WEBP]
            quality: 100
          )
        }
      }
    }
  `)
  return (
    <Layout>
      <Seo title="文化伴手禮" />

      <div>
        <h2
          style={{
            textAlign: "center",
            fontSize: "36px",
            color: "#325346",
          }}
        >
          鰲峰品茗茶包組
        </h2>
        <Slider {...settings}>
          <div>
            <GatsbyImage
              image={images["tea1"]?.childImageSharp?.gatsbyImageData}
            />
          </div>
          <div>
            <GatsbyImage
              image={images["tea2"]?.childImageSharp?.gatsbyImageData}
            />
          </div>
          <div>
            <GatsbyImage
              image={images["tea3"]?.childImageSharp?.gatsbyImageData}
            />
          </div>
          <div>
            <GatsbyImage
              image={images["tea4"]?.childImageSharp?.gatsbyImageData}
            />
          </div>
        </Slider>

        <p>
          <strong>口味</strong>：金萱茶、紅茶、鐵觀音、東方美人茶
        </p>
        <p>
          <strong>創作理念</strong>
          ：我們將茶與陳錫金先生的鰲峰竹枝詞做結合，以竹枝詞中呈現的意象景趣，來傳達每一種茶的特色、形象與口感，在品茗的過程中，也能增加想像畫面上的美感，體會鰲峰當地舊時的人情風貌。
        </p>
        <p>
          <strong>竹枝詞作者資訊：</strong>
          陳錫金（1867～1935），字基六，號式金，又號蟄村，晚號蟄翁，臺中牛罵頭（今臺中清水）人。晚清生員。日治時期嘗任高美區（今清水）區長及《臺灣新聞報》記者。擅中醫，有名於杏林。性好吟詠，明治三十五年（1902）加入「櫟社」，為該社創社九老之一；又曾組清水「鰲西詩社」「戊午」（1918）。
        </p>
      </div>
      <div className="tea-wrapper">
        <div className="tea">
          <div className="title">萱茗醇香（金萱茶）</div>
          <div className="content">
            <div className="desc">
              <p>陳錫金－鰲峰竹枝詞（十六首之四）</p>
              <p>元宵女伴互歡呼，清茗一杯香一爐。</p>
              <p>花粉檳榔盡羅列，燈前準備問三姑。</p>
            </div>
            <div className="more">
              <p className="title">命名用意</p>
              <p>
                金萱茶有著獨特的奶香味，若是製作成包種茶或烏龍茶，入口則會有著獨特的牛奶香氣在口中蔓延開來，香氣雖重，卻不失其茶味。而正因為金萱茶有此特性，更是得到了眾多人的喜愛，結合上鰲峰竹枝詞之四的元宵歡慶的場景，更是不謀而合。
              </p>
              <p className="title">產品介紹</p>
              <p>
                金萱茶為1981年培育出來的茶樹品種，其樹型橫張型，葉型橢圓形，葉子肥厚、鮮嫩，茶葉色澤翠綠富光澤，芽綠中帶紫、茸毛，芽密度高，採摘期長，抗枝枯病。
                此茶種主要產地在南投及嘉義，種植面積在穩定的增加中。由於樹勢強健，環境適應力強，產量高，因此全國各茶區均有種。
              </p>
            </div>
          </div>
        </div>

        <div className="tea">
          <div className="title">花蹤迷紅(紅茶)</div>
          <div className="content">
            <div className="desc">
              <p>陳錫金－鰲峰竹枝詞（十六首之五)</p>
              <p>遊春人唱冶春詞，奼紫嫣紅繫綺思。</p>
              <p>商略蹈青何處好，萋萋花草虎頭崎。</p>
            </div>
            <div className="more">
              <p className="title">命名用意</p>
              <p>
                花蹤迷紅，猶如走在春天滿天花朵的小徑，香氣四溢，氣味使人流連忘返，猶如已與花融為一體；金紅色澤的紅茶甘澄醇厚，彷彿已沉浸在其紅色氛圍久久無法忘懷，春天百花漸漸萌發，一路上各種花色點綴，蟲鳴鳥叫此起彼落，也正是春遊陶冶性情的最佳時機。此等情趣就如飲用紅茶所帶來的味覺盛宴一樣豐富、繽紛。紅茶富含花果、花香，午後、清晨、外出時，特別是在旅遊、飽覽美景時，若能飲用澄紅色澤的紅茶，將帶來視覺和味覺及嗅覺三種衝擊，帶來不痛的盛典。
              </p>
              <p className="title">產品介紹</p>
              <p>
                此紅茶為台灣原生種的野生茶樹，種植於臺灣的高山上，吸取台灣高山的靈氣及養分，植株較為稀少，恆溫發酵後，沖入滾水，甘醇度和香氣皆領先於其他茶類，蘊含成熟果香及淡雅花香，乃紅茶中的極品。
                <br />
                春天的紅茶會有一股花果香，含有層次韻味的香氣環繞其中。來自臺灣的紅茶，茶湯金中帶紅，澄透熟純、喉韻甜潤，輕酌而下的瞬間，彷彿臺灣春天的美景、戚戚芳草、百花綻放在舌尖上，耳里聽到的不再是將甘醇紅茶吞嚥至喉中的聲音，而是盎然的春天交響樂。
              </p>
            </div>
          </div>
        </div>

        <div className="tea">
          <div className="title">觀音雅禪(鐵觀音)</div>
          <div className="content">
            <div className="desc">
              <p>陳錫金－鰲峰竹枝詞（十六首之十）</p>
              <p>兒家禪悅味偏濃，參拜觀音自肅容。</p>
              <p>昨日覺元堂裡去，喜聽一百八聲鐘。</p>
            </div>
            <div className="more">
              <p className="title">命名用意</p>
              <p>
                鐵觀音以炭火精焙，色澤澄如琥珀，茶香濃烈，品茗醇厚，入喉回甘。飲之獨特花香，心曠神怡，如臨寂靜之地，超脫凡塵，洗滌身心的髒污，忘卻現實的煩惱，只需沉澱心靈，停止雜念，享受當下。
              </p>
              <p className="title">產品介紹</p>
              <p>
                鐵觀音是一種茶樹名稱，通常用來製作成烏龍茶，其成品也稱作鐵觀音。鐵觀音「茶色澤烏潤、沉重似鐵」故稱之，茶的香氣馥郁，入口醇厚，立即回甘帶蜜，輕茗一口，瞬間忘記一切世俗雜事，只想好好沉浸在鐵觀音帶來的味蕾饗宴。
              </p>
            </div>
          </div>
        </div>

        <div className="tea">
          <div className="title">翠眉翩躚(東方美人茶)</div>
          <div className="content">
            <div className="desc">
              <p>陳錫金－鰲峰竹枝詞（十六首之十三）</p>
              <p>水晶簾捲玉樓秋，眉黛親描一段愁。</p>
              <p>誰供翠螺三十斛，柴梳山色映梳頭。</p>
            </div>
            <div className="more">
              <p className="title">命名用意</p>
              <p>
                白毫烏龍又稱東方美人茶，鮮嫩細小的茶芽隨著熱氣蒸騰在水中舞躍，猶如絕色美人在英倫的水晶杯中翩躚起舞，以此聞名。翠眉，亦指美人。蜷起的茶葉在水的呵護下舒展，猶如輕蹙的眉間憂愁散盡，似嘗到蜜的香甜後展開笑顏地那般嫵媚，帶著融入茶水中的翠色，翩躚。命名與竹枝詞意象相吻。
              </p>
              <p className="title">產品介紹</p>
              <p>
                東方美人茶的採收在炎夏六、七月，大約是農曆芒種至大暑間。它最著名的地方，就是那悠長細膩的蜂蜜香和熟果香，這樣的香氣之所以會產生，是由於茶樹長出的嫩芽經過茶小綠葉蟬(小綠浮塵子)蜜吻，這個過程稱為「著涎」，茶葉品質的好壞也取決於此。
                東方美人茶製茶，採用細嫩的新芽，茶湯滋味甘甜爽口，又因低溫炒菁、乾燥，所以沒有焙火味，不苦也不澀。此外，因重發酵的緣故，成茶外觀呈現白、綠、黃、紅、褐色，五色相間，又名「五色茶」。
              </p>
            </div>
          </div>
        </div>
      </div>
      <Link className="back" to="/">
        返回首頁
      </Link>
    </Layout>
  )
}

export default Page8
